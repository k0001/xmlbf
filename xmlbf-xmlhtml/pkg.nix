{ mkDerivation, base, bytestring, html-entities, lib, tasty
, tasty-hunit, text, unordered-containers, xmlbf, xmlhtml
}:
mkDerivation {
  pname = "xmlbf-xmlhtml";
  version = "0.2.2";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring html-entities text unordered-containers xmlbf
    xmlhtml
  ];
  testHaskellDepends = [ base tasty tasty-hunit xmlbf ];
  homepage = "https://gitlab.com/k0001/xmlbf";
  description = "xmlhtml backend support for the xmlbf library";
  license = lib.licenses.asl20;
}
