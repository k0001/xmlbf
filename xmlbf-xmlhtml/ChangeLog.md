# 0.2.2

* Updated to work with `xmlbf-0.7`.

# 0.2.1

* Removed unnecessary test dependencies.


# 0.2

* COMPILER ASSISTED BREAKING CHANGE. `element` renamed to
  `fromXmlHtmlNode`.

* COMPILER ASSISTED BREAKING CHANGE. `element'` removed.

* COMPILER ASSISTED BREAKING CHANGE. `nodesXml` renamed to
  `fromRawXml`.

* COMPILER ASSISTED BREAKING CHANGE. `nodesHtml` renamed to
  `fromRawHtml`.

* Made compatible with `xmlbf-0.5`.


# 0.1.1

* Made compatible with `xmlbf-0.3`.


# 0.1

* First version.
