{ mkDerivation, base, bytestring, containers, deepseq, QuickCheck
, quickcheck-instances, selective, lib, tasty, tasty-hunit, mmorph
, tasty-quickcheck, text, transformers, unordered-containers, mtl
, exceptions
}:
mkDerivation {
  pname = "xmlbf";
  version = "0.7";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring containers deepseq selective text transformers
    unordered-containers mtl mmorph exceptions
  ];
  testHaskellDepends = [
    base bytestring QuickCheck quickcheck-instances tasty tasty-hunit
    tasty-quickcheck text transformers
  ];
  homepage = "https://gitlab.com/k0001/xmlbf";
  description = "XML back and forth! Parser, renderer, ToXml, FromXml, fixpoints";
  license = lib.licenses.asl20;
}
