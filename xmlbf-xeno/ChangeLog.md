# 0.2.2

* Updated to work with `xmlbf-0.7`.

# 0.2.1

* Added benchmark.

* Removed unnecessary test dependencies.


# 0.2

* COMPILER ASSISTED BREAKING CHANGE. Renamed `element` to `fromXenoNode`.

* COMPILER ASSISTED BREAKING CHANGE. Renamed `nodes` to `fromRawXml`.

* Made compatible with `xmlbf-0.5`.

* Leading UTF-8 BOM safely dealt with in `fromRawXml`. See issue #20.


# 0.1.1

* Made compatible with `xmlbf-0.3`.


# 0.1

* First version.
